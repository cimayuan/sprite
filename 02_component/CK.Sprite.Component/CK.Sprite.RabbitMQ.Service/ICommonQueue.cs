﻿using System;
using System.Threading.Tasks;
using CK.Sprite.RabbitMQ.Core;

namespace CK.Sprite.RabbitMQ.Service
{
    public interface ICommonQueue : IRunnable, IDisposable
    {
        Task<string> EnqueueAsync(
            object args,
            byte priority = 5,
            TimeSpan? delay = null
        );
    }
}