﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using CK.Sprite.RabbitMQ.Core;
using CK.Sprite.RabbitMQ.Service;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionRabbitMQExtensions
    {
        public static IServiceCollection AddRabbitMQCore(this IServiceCollection services)
        {
            // CommonQueueManager : ICommonQueueManager
            services = services.AddSingleton<IChannelPool, ChannelPool>();
            services = services.AddSingleton<IConnectionPool, ConnectionPool>();
            services = services.AddSingleton<IRabbitMqMessageConsumerFactory, RabbitMqMessageConsumerFactory>();

            services = services.AddTransient<IRabbitMqMessageConsumer, RabbitMqMessageConsumer>();
            services = services.AddTransient<IRabbitMqSerializer, Utf8JsonRabbitMqSerializer>();
            services = services.AddTransient<AbpTimer>();

            services = services.AddSingleton<ICommonQueueManager, CommonQueueManager>();

            return services;
        }
    }
}
