﻿using System;
using CK.Sprite.RabbitMQ.Core;

namespace CK.Sprite.RabbitMQ.Service
{
    public class CommonQueueConfiguration : QueueDeclareConfiguration
    {
        public string ConnectionName { get; set; }

        public CommonQueueConfiguration(
            string queueName,
            string connectionName = null,
            bool durable = true,
            bool exclusive = false,
            bool autoDelete = false)
            : base(
                queueName,
                durable,
                exclusive,
                autoDelete)
        {
            ConnectionName = connectionName;
        }
    }
}