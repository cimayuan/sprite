﻿/*
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 * 
 */

using Aliyun.OSS.NetCore.Model;

namespace Aliyun.OSS.NetCore
{
    /// <summary>
    /// The result class of the operation to get bucket's location.
    /// </summary>
    public class BucketLocationResult : GenericResult
    {
        /// <summary>
        /// The bucket location.
        /// </summary>
        public string Location { get; internal set; }
    }
}
