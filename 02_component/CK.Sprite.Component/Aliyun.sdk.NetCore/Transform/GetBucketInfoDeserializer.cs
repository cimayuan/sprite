﻿/*
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 * 
 */

using System;
using System.IO;
using Aliyun.OSS.NetCore.Common.Communication;
using Aliyun.OSS.NetCore.Util;
using Aliyun.OSS.NetCore.Model;

namespace Aliyun.OSS.NetCore.Transform
{
    internal class GetBucketInfoDeserializer : ResponseDeserializer<BucketInfo, BucketInfo>
    {
        public GetBucketInfoDeserializer(IDeserializer<Stream, BucketInfo> contentDeserializer)
            : base(contentDeserializer)
        { }

        public override BucketInfo Deserialize(ServiceResponse xmlStream)
        {
            return ContentDeserializer.Deserialize(xmlStream.Content);
        }
    }
}
