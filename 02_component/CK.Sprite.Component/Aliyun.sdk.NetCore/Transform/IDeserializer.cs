﻿/*
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 * 
 */

using Aliyun.OSS.NetCore.Model;

namespace Aliyun.OSS.NetCore.Transform
{
    internal interface IDeserializer<in TInput, out TOutput>
    {
        TOutput Deserialize(TInput xmlStream);
    }
}
