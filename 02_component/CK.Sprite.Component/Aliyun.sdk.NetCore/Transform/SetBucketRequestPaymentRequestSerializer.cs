﻿/*
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 * 
 */

using System.IO;
using Aliyun.OSS.NetCore.Common.Communication;
using Aliyun.OSS.NetCore.Model;
using Aliyun.OSS.NetCore.Util;

namespace Aliyun.OSS.NetCore.Transform
{
    internal class SetBucketRequestPaymentRequestSerializer : RequestSerializer<SetBucketRequestPaymentRequest, RequestPaymentConfiguration>
    {
        public SetBucketRequestPaymentRequestSerializer(ISerializer<RequestPaymentConfiguration, Stream> contentSerializer)
            : base(contentSerializer)
        {
        }

        public override Stream Serialize(SetBucketRequestPaymentRequest request)
        {
            var model = new RequestPaymentConfiguration();
            model.Payer = request.Payer;
            return ContentSerializer.Serialize(model);
        }
    }
}

