﻿/*
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 * 
 */

using System;
using System.IO;
using Aliyun.OSS.NetCore.Common.Communication;
using Aliyun.OSS.NetCore.Util;
using Aliyun.OSS.NetCore.Model;

namespace Aliyun.OSS.NetCore.Transform
{
    internal class GetBucketStatDeserializer : ResponseDeserializer<BucketStat, BucketStat>
    {
        public GetBucketStatDeserializer(IDeserializer<Stream, BucketStat> contentDeserializer)
            : base(contentDeserializer)
        { }

        public override BucketStat Deserialize(ServiceResponse xmlStream)
        {
            return ContentDeserializer.Deserialize(xmlStream.Content);
        }
    }
}
