using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace CK.Sprite.MessageComponent
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddMessages(this IServiceCollection services, Action<OptionsBuilder<SmtpOptions>> options = null)
        {
            var optionsBuilder = services.AddOptions<SmtpOptions>();
            options?.Invoke(optionsBuilder);
            
            services.AddSingleton<ISmtpService, SmtpService>();
            services.AddSingleton<IAliyunShortMessage, AliyunShortMessage>();

            return services;
        }
    }
}