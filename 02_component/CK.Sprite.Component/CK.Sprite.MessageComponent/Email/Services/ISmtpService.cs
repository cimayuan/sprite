﻿using System.Threading;
using System.Threading.Tasks;
using MimeKit;

namespace CK.Sprite.MessageComponent
{
    public interface ISmtpService
    {
        Task<SendResultEntity> SendEmailAsync(MailEntity mailEntity, CancellationToken cancellationToken = default);
    }
}