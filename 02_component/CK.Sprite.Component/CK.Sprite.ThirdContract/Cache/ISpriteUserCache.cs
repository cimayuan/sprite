﻿using CK.Sprite.Cache;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    public interface ISpriteUserCache : ILocalCache<SpriteUser>
    {
        Task<SpriteUser> GetById(string id);

         Task<List<SpriteUser>> GetByIds(IEnumerable<string> ids);

         Task<List<SpriteUser>> GetByIdsAndDeptId(IEnumerable<string> ids, string deptId);
    }

    public interface IRemoteSpriteUserService
    {
        List<SpriteUser> GetAll();
    }
}
