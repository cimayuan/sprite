﻿using CK.Sprite.Cache;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    public interface ISpriteUserRoleCache : ILocalCache<SpriteUserRole>
    {
        /// <summary>
        /// 根据角色Id获取用户Id集合
        /// </summary>
        /// <param name="roleId">角色Id</param>
        /// <returns></returns>
        Task<List<string>> GetUserIdsByRoleId(string roleId);
    }

    public interface IRemoteSpriteUserRoleService
    {
        List<SpriteUserRole> GetAll();
    }
}
