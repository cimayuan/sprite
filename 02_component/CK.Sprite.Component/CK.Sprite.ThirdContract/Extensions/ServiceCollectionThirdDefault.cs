﻿using CK.Sprite.CrossCutting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    public static class ServiceCollectionThirdDefault
    {
        public static void AddThirdDefault(this IServiceCollection services)
        {
            services.AddSingleton(typeof(IThirdService), typeof(LocalCacheThirdService));
            services.AddSingleton(typeof(ISpriteDeptCache), typeof(LocalSpriteDeptCache));
            services.AddSingleton(typeof(ISpriteRoleCache), typeof(LocalSpriteRoleCache));
            services.AddSingleton(typeof(ISpriteUserCache), typeof(LocalSpriteUserCache));
            services.AddSingleton(typeof(ISpriteUserRoleCache), typeof(LocalSpriteUserRoleCache));
            services.AddSingleton(typeof(IMessageSendService), typeof(MessageSendService));
        }

        public static void AddApiThirdRemoteService(this IServiceCollection services)
        {
            services.AddSingleton(typeof(IRemoteSpriteDeptService), typeof(ApiThirdDeptRemoteService));
            services.AddSingleton(typeof(IRemoteSpriteUserService), typeof(ApiThirdUserRemoteService));
            services.AddSingleton(typeof(IRemoteSpriteRoleService), typeof(ApiThirdRoleRemoteService));
            services.AddSingleton(typeof(IRemoteSpriteUserRoleService), typeof(ApiThirdUserRoleRemoteService));
        }

        public static void AddApiFormThirdServiceAppService(this IServiceCollection services)
        {
            services.AddSingleton(typeof(IFormThirdServiceAppService), typeof(ApiFormThirdServiceAppService));
        }

        public static void AddApiWorkflowThirdService(this IServiceCollection services)
        {
            services.AddSingleton(typeof(IWorkflowThirdService), typeof(ApiWorkflowThirdService));
        }

        public static void AddTestCurrentInfo(this IServiceCollection services)
        {
            services.AddTransient(typeof(CK.Sprite.ThirdContract.ICurrentUser), typeof(DefaultCurrentUser));
            services.AddTransient(typeof(CK.Sprite.ThirdContract.ICurrentTenant), typeof(DefaultCurrentTenant));
        }
    }
}
