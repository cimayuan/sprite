using System.Collections.Generic;
using System.Linq;
using CK.Sprite.Cache;
using CK.Sprite.ThirdContract;
using CK.Sprite.CrossCutting;
using Microsoft.Extensions.Options;
using System.Net.Http;
using System;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Text;
using CK.Sprite.Framework;

namespace CK.Sprite.ThirdContract
{
    public class ApiFormThirdServiceAppService : LazyService, IFormThirdServiceAppService
    {
        public SpriteConfig _callHttpConfig => LazyGetRequiredService(ref callHttpConfig).Value;
        private IOptions<SpriteConfig> callHttpConfig;

        public async Task<object> DoRuntimeMethod(JObject paramObject)
        {
            using (var client = new HttpClient())
            {
                var url = _callHttpConfig.FormUrl + "/api/spriteform/common/DoRuntimeMethod";

                var response = await client.PostAsync(
                    url,
                    new StringContent(
                        JsonConvert.SerializeObject(paramObject),
                        Encoding.UTF8,
                        "application/json"
                    )
                );

                if (response.IsSuccessStatusCode)
                {
                    var responseContent = await response.Content.ReadAsStringAsync();
                    return responseContent;
                }
                else
                {
                    throw new SpriteException("调用表单方法失败");
                }
            }
        }
    }
}
