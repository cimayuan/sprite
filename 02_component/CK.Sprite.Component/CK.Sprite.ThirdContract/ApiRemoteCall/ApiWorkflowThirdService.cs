using System.Collections.Generic;
using System.Linq;
using CK.Sprite.Cache;
using CK.Sprite.ThirdContract;
using CK.Sprite.CrossCutting;
using Microsoft.Extensions.Options;
using System.Net.Http;
using System;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Text;
using CK.Sprite.Framework;

namespace CK.Sprite.ThirdContract
{
    public class ApiWorkflowThirdService : LazyService, IWorkflowThirdService
    {
        public SpriteConfig _callHttpConfig => LazyGetRequiredService(ref callHttpConfig).Value;
        private IOptions<SpriteConfig> callHttpConfig;

        public async Task<List<FormWorkflowInfo>> GetFormWorkflowInfos(List<string> instanceIds)
        {
            if(instanceIds.Count == 0)
            {
                return new List<FormWorkflowInfo>();
            }
            using (var client = new HttpClient())
            {
                var url = _callHttpConfig.WorkflowUrl + "/api/workflow/workflowRuntime/GetFormWorkflowInfos";

                var response = await client.PostAsync(
                    url,
                    new StringContent(
                        JsonConvert.SerializeObject(instanceIds),
                        Encoding.UTF8,
                        "application/json"
                    )
                );

                if (response.IsSuccessStatusCode)
                {
                    var responseContent = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<List<FormWorkflowInfo>>(responseContent);
                }
                else
                {
                    throw new SpriteException("���÷���ʧ��");
                }
            }
        }
    }
}
