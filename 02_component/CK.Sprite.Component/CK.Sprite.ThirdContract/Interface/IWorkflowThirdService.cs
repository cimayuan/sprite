﻿using CK.Sprite.Framework;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    public interface IWorkflowThirdService
    {
        /// <summary>
        /// 获取表单关联流程信息
        /// </summary>
        /// <param name="instanceIds">流程实例Id集合</param>
        /// <returns></returns>
        Task<List<FormWorkflowInfo>> GetFormWorkflowInfos(List<string> instanceIds);
    }
}
