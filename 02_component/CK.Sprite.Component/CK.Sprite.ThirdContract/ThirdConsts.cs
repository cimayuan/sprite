﻿using CK.Sprite.Cache;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    public class ThirdConsts
    {
        public const string SpriteUserCacheKey = "Third_SpriteUserCacheKey";
        public const string SpriteDeptCacheKey = "Third_SpriteDeptCacheKey";
        public const string SpriteRoleCacheKey = "Third_SpriteRoleCacheKey";
        public const string SpriteUserRoleCacheKey = "Third_SpriteUserRoleCacheKey";
        public const string FrameworkCachePreKey = "Framework";
        public const string ThirdCachePreKey = "Third";
    }
}
