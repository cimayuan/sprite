﻿using System.Collections.Generic;

namespace CK.Sprite.Cache
{
    /// <summary>
    /// 设计时实体变更通知缓存
    /// </summary>
    public interface ICacheSendNotice
    {
        /// <summary>
        /// 发送缓存变更
        /// </summary>
        /// <param name="key">缓存Key</param>
        void SendClearCache(string key);

        /// <summary>
        /// 发送缓存多个变更
        /// </summary>
        /// <param name="key">缓存Key集合</param>
        void SendClearCaches(List<string> keys);
    }
}
