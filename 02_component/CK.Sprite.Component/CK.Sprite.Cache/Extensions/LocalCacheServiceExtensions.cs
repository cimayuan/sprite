﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CK.Sprite.Cache
{
    public static class LocalCacheServiceExtensions
    {
        public static IServiceCollection AddLocalCacheSendNotice(this IServiceCollection services)
        {
            return services.AddSingleton(typeof(ICacheSendNotice), typeof(LocalCacheSendNotice));
        }
    }
}
