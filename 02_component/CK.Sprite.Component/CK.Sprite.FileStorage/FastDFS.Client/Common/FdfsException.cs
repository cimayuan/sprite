﻿using System;

namespace FastDFS.Client
{
   
    public class FdfsException : Exception
    {
        public FdfsException()
        {
        }

        public FdfsException(string message)
            : base(message)
        {

        }
    }
}
