﻿using CK.Sprite.FileStorage.Application;

namespace CK.Sprite.FileStorage
{
    /// <summary>
    /// configurations of FastDFS client
    /// </summary>
    public class FastDfsOptions: FileStorgeOptions
    {
        /// <summary>
        /// all tracker ip(s) and port
        /// </summary>
        public string[] Trackers { get; set; }

        /// <summary>
        /// wether to enable local storage provider
        /// </summary>
        public bool EnableLocalStorageProvider { get; set; } = false;



    }
}