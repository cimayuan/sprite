﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CK.Sprite.FileStorage.Application
{
    public class FileStorgeOptions
    {

        /// <summary>
        /// domain of http, with port.
        /// </summary>
        public string HttpDomain { get; set; } = string.Empty;

        /// <summary>
        /// local storage absolute path
        /// </summary>
        public string LocalStoragePathParent { get; set; }

        /// <summary>
        /// local storage relative path to <see cref="LocalStoragePathParent"/>
        /// </summary>
        public string LocalStoragePath { get; set; }

        /// <summary>
        /// 存储服务器类型
        /// </summary>
        public EStoreType StoreType { get; set; }

    }
}
