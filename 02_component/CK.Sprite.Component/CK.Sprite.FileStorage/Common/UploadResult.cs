﻿using System;
using System.Collections.Generic;

namespace CK.Sprite.FileStorage
{
    public class UploadResult
    {       
        /// <summary>
        /// 上传结果状态码
        /// </summary>
        public int code { get; set; }

        /// <summary>
        /// 上传结果消息
        /// </summary>
        public string message { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public List<UploadFileInfo> files { get; set; }
    }
}