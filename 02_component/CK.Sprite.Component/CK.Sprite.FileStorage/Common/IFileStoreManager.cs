﻿using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CK.Sprite.FileStorage
{
    public interface IFileStoreManager
    {
        /// <summary>
        /// 获取所有存储配置信息
        /// </summary>
        /// <returns></returns>
        Task<List<StoreInfo>> GetStoreInfos();
    }

    public class LocalFileStoreManager : IFileStoreManager
    {
        private readonly StoreInfoOptions _storeInfoOptions;

        public LocalFileStoreManager(IOptions<StoreInfoOptions> storeInfoOptions)
        {
            _storeInfoOptions = storeInfoOptions.Value;
        }

        public Task<List<StoreInfo>> GetStoreInfos()
        {
            var result = _storeInfoOptions.StoreInfos;

            return Task.FromResult(result);
        }
    }

    public class StoreInfoOptions
    {
        public List<StoreInfo> StoreInfos { get; set; }
    }
}