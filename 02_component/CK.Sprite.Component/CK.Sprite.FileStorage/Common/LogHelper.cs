﻿namespace CK.Sprite.FileStorage
{
    public interface ILogHelper
    {
        void LogInformation(string message);
        void LogError(string message);
        void LogDebug(string message);
    }

    internal class NullFileLogHelper : ILogHelper
    {
        public void LogDebug(string message)
        {

        }

        public void LogError(string message)
        {
        }

        public void LogInformation(string message)
        {
        }
    }

    public static class LogHelper
    {
        internal static ILogHelper FileLogHelper { get; private set; }

        public static void InitLogHelper(ILogHelper logHelper)
        {
            FileLogHelper = logHelper;
        }
    }
}