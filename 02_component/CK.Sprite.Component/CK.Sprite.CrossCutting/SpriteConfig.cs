﻿using System.Collections.Generic;

namespace CK.Sprite.CrossCutting
{
    public class SpriteConfig
    {
        public string MessageUrl { get; set; }
        public string IdentityUrl { get; set; }
        public string FormUrl { get; set; }
        public string WorkflowUrl { get; set; }
        public List<string> RemoteNoticePreKey { get; set; }
        public List<string> RemoteReceivePreKey { get; set; }
    }
}
