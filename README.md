# sprite

#### 介绍
.net core 自定义表单引擎

#### 软件架构

- 架构设计参考：https://www.cnblogs.com/spritekuang/p/14032899.html
- 表单应用案例：https://www.cnblogs.com/spritekuang/p/14970986.html
- 工作流自定义表单应用案例：https://www.cnblogs.com/spritekuang/p/14970992.html
- 开源地址：https://gitee.com/kuangqifu/sprite
- 体验地址，（首次加载可能有点慢，用的阿里云最差的服务器）：http://47.108.141.193:8031
- 自定义表单文章地址：https://www.cnblogs.com/spritekuang/
- 流程引擎文章地址（采用WWF开发，已过时，已改用Elsa实现）：https://www.cnblogs.com/spritekuang/category/834975.html
- Github地址：https://github.com/kuangqifu/CK.Sprite.Job
- QQ交流：523477776



#### 使用说明


1.  还原数据库test_20210312.sql(里面包含了工作流相关表，可以不管)
2.  修改数据库连接，运行项目(或者直接运行，数据库外网可用)
3.  登录，体验地址，F12进入调试模式，参照各个表单测试表单业务模块调用相关接口 http://47.108.141.193:8031



#### 特殊说明

1.  自定义表单的所有后端功能都已经包含进来了，可以参照体验地址调用相关接口理解这部分设计，真实项目中后端开发的大部分基础功能完全可以省略。
2.  后续要做的是把表单引擎集成到自己的开发框架了，我这边是集成到Abp VNext开发框架中。
3.  自定义表单引擎前端部分也是非常重要且复杂的，不要用Angular来实现，限制太多，因为与我这边的开发框架强依赖，剥离出来要另外花些时间，暂时没有开源出来，后续看情况是否开源。
4.  流程引擎最开始采用WWF开发，现在采用Elsa全部重新开发，比市面上绝大多数流程引擎更加轻量且实现更强大功能，暂时不开源，流程引擎这块自己理解还是比较深刻，对这块感觉兴趣可私下交流。
