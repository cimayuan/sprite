﻿using CK.Sprite.Form.Core;
using CK.Sprite.Framework;
using Dapper;
using Dapper.Contrib.Extensions;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Business
{
    public interface IStockRepository : IRepository
    {
        /// <summary>
        /// 删除数据库中检查天盘点数据
        /// </summary>
        /// <param name="checkDate">盘点日期</param>
        /// <returns></returns>
        Task RemoveStockChecksAsync(DateTime checkDate);

        /// <summary>
        /// 获取库存盘点数据
        /// </summary>
        /// <param name="fields">统计字段</param>
        /// <param name="checkTime">盘点时间</param>
        /// <returns></returns>
        Task<JObject> GetStockChecks(List<string> fields, DateTime checkTime);

        /// <summary>
        /// 获取当日盘点数据
        /// </summary>
        /// <param name="fields">动态统计字段</param>
        /// <param name="checkTime">盘点时间</param>
        /// <param name="stockType">in或者out字典</param>
        /// <returns></returns>
        Task<JObject> GetStockCheckDays(List<string> fields, DateTime checkTime, string stockType);

        /// <summary>
        /// 添加库存盘点
        /// </summary>
        /// <param name="fields">动态新增字段</param>
        /// <param name="addData">新增数据实体</param>
        /// <returns></returns>
        Task AddStockChecksAsync(List<string> fields, JObject addData);
    }
}
