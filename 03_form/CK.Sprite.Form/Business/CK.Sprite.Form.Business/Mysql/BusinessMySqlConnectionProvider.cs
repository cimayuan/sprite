﻿using CK.Sprite.Form.Core;
using CK.Sprite.Framework;
using Microsoft.Extensions.DependencyInjection;
using MySql.Data.MySqlClient;
using System;
using System.Data;

namespace CK.Sprite.Form.Business
{
    public class BusinessMySqlConnectionProvider : IConnectionProvider
    {
        public IDbConnection CreateDbConnection(string strConn)
        {
            return new MySqlConnection(strConn);
        }

        public T GetRepository<T>(IUnitOfWork unitOfWork) where T:class, IRepository
        {
            if(typeof(T).IsAssignableFrom(typeof(IStockRepository)))
            {
                return new StockRepository(unitOfWork) as T;
            }

            return default(T);
        }
    }
}
