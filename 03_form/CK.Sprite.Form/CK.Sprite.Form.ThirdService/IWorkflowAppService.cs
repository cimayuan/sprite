﻿using CK.Sprite.Framework;
using Newtonsoft.Json.Linq;
using System;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.ThirdService
{
    public interface IWorkflowAppService : IAppService
    {
        /// <summary>
        /// 运行时动态执行有一份
        /// </summary>
        /// <param name="paramObject">执行方法集合</param>
        /// <returns></returns>
        Task<object> GetInstanceInfo(JObject paramObject);
    }
}
