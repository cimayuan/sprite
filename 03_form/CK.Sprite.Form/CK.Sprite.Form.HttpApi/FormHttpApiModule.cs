﻿using AutoMapper;
using CK.Sprite.Framework;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace CK.Sprite.Form.Core
{
    public class FormHttpApiModule : SpriteModule
    {
        public override void PreConfigureServices(IServiceCollection Services)
        {
            Services.AddAssemblyOf<FormHttpApiModule>();
        }
    }
}
