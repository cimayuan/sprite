﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CK.Sprite.Form.Core;
using Newtonsoft.Json.Linq;
using CK.Sprite.Framework;

namespace CK.Sprite.Form.Controllers
{
    [ApiController]
    [Area("spriteform")]
    [ControllerName("Dict")]
    [Route("api/spriteform/dict")]
    public class DictController : Controller, ITransientDependency
    {
        private readonly IDictAppService _dictAppService;

        public DictController(IDictAppService dictAppService)
        {
            _dictAppService = dictAppService;
        }

        [HttpPost]
        [Route("AddDict")]
        public async Task AddDict(CreateDictDto dictDto)
        {
            await _dictAppService.AddDict(dictDto);
        }

        [HttpPost]
        [Route("AddDictItem")]
        public async Task AddDictItem(CreateDictItemDto dictItemDto)
        {
            await _dictAppService.AddDictItem(dictItemDto);
        }

        [HttpGet]
        [Route("DeleteDict")]
        public async Task DeleteDict(Guid id)
        {
            await _dictAppService.DeleteDict(id);
        }

        [HttpGet]
        [Route("DeleteDictItem")]
        public async Task DeleteDictItem(Guid id)
        {
            await _dictAppService.DeleteDictItem(id);
        }

        [HttpPost]
        [Route("UpdateDict")]
        public async Task UpdateDict(DictDto dictDto)
        {
            await _dictAppService.UpdateDict(dictDto);
        }

        [HttpPost]
        [Route("UpdateDictItem")]
        public async Task UpdateDictItem(DictItemDto dictItemDto)
        {
            await _dictAppService.UpdateDictItem(dictItemDto);
        }

        [HttpGet]
        [Route("GetDictList")]
        public async Task<List<DictDto>> GetDictList()
        {
            return await _dictAppService.GetDictList();
        }

        [HttpGet]
        [Route("GetDictItemList")]
        public async Task<List<DictItemDto>> GetDictItemList(string dictCode)
        {
            return await _dictAppService.GetDictItemList(dictCode);
        }
    }
}
