﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CK.Sprite.Form.Core;
using CK.Sprite.Framework;

namespace CK.Sprite.Form.Controllers
{
    [ApiController]
    [Area("spriteform")]
    [ControllerName("Common")]
    [Route("api/spriteform/common")]
    public class CommonController : Controller, ITransientDependency
    {
        private readonly ICommonAppService _commonAppService;

        public CommonController(ICommonAppService commonAppService)
        {
            _commonAppService = commonAppService;
        }

        [HttpPost]
        [Route("AddControl")]
        public async Task AddControl(ControlCreateDto controlCreateDto)
        {
            await _commonAppService.AddControl(controlCreateDto);
        }

        [HttpPost]
        [Route("AddRuleAction")]
        public async Task AddRuleAction(RuleActionCreateDto ruleActionCreateDto)
        {
            await _commonAppService.AddRuleAction(ruleActionCreateDto);
        }

        [HttpPost]
        [Route("AddSpriteRule")]
        public async Task AddSpriteRule(SpriteRuleCreateDto spriteRuleCreateDto)
        {
            await _commonAppService.AddSpriteRule(spriteRuleCreateDto);
        }

        [HttpPost]
        [Route("AddWrapInfo")]
        public async Task AddWrapInfo(WrapInfoCreateDto wrapInfoCreateDto)
        {
            await _commonAppService.AddWrapInfo(wrapInfoCreateDto);
        }

        [HttpGet]
        [Route("DeleteControl")]
        public async Task DeleteControl(Guid id)
        {
            await _commonAppService.DeleteControl(id);
        }

        [HttpGet]
        [Route("DeleteRuleAction")]
        public async Task DeleteRuleAction(Guid id)
        {
            await _commonAppService.DeleteRuleAction(id);
        }

        [HttpGet]
        [Route("DeleteSpriteRule")]
        public async Task DeleteSpriteRule(Guid id)
        {
            await _commonAppService.DeleteSpriteRule(id);
        }

        [HttpGet]
        [Route("DeleteWrapInfo")]
        public async Task DeleteWrapInfo(Guid id)
        {
            await _commonAppService.DeleteWrapInfo(id);
        }

        [HttpGet]
        [Route("GetListControlAsync")]
        public async Task<List<ControlDto>> GetListControlAsync(Guid businessId)
        {
            return await _commonAppService.GetListControlAsync(businessId);
        }

        [HttpGet]
        [Route("GetControlByIdAsync")]
        public async Task<ControlDto> GetControlByIdAsync(Guid id)
        {
            return await _commonAppService.GetControlByIdAsync(id);
        }

        [HttpGet]
        [Route("GetListRuleActionAsync")]
        public async Task<List<RuleActionDto>> GetListRuleActionAsync(Guid ruleId)
        {
            return await _commonAppService.GetListRuleActionAsync(ruleId);
        }

        [HttpGet]
        [Route("GetRuleActionByIdAsync")]
        public async Task<RuleActionDto> GetRuleActionByIdAsync(Guid id)
        {
            return await _commonAppService.GetRuleActionByIdAsync(id);
        }

        [HttpGet]
        [Route("GetListSpriteRuleAsync")]
        public async Task<List<SpriteRuleDto>> GetListSpriteRuleAsync(Guid businessId)
        {
            return await _commonAppService.GetListSpriteRuleAsync(businessId);
        }

        [HttpGet]
        [Route("GetSpriteRuleByIdAsync")]
        public async Task<SpriteRuleDto> GetSpriteRuleByIdAsync(Guid id)
        {
            return await _commonAppService.GetSpriteRuleByIdAsync(id);
        }

        [HttpGet]
        [Route("GetListWrapInfoAsync")]
        public async Task<List<WrapInfoDto>> GetListWrapInfoAsync(Guid businessId)
        {
            return await _commonAppService.GetListWrapInfoAsync(businessId);
        }

        [HttpGet]
        [Route("GetWrapInfoByIdAsync")]
        public async Task<WrapInfoDto> GetWrapInfoByIdAsync(Guid id)
        {
            return await _commonAppService.GetWrapInfoByIdAsync(id);
        }

        [HttpPost]
        [Route("UpdateControl")]
        public async Task UpdateControl(ControlUpdateDto controlUpdateDto)
        {
            await _commonAppService.UpdateControl(controlUpdateDto);
        }

        [HttpPost]
        [Route("UpdateRuleAction")]
        public async Task UpdateRuleAction(RuleActionUpdateDto ruleActionUpdateDto)
        {
            await _commonAppService.UpdateRuleAction(ruleActionUpdateDto);
        }

        [HttpPost]
        [Route("UpdateSpriteRule")]
        public async Task UpdateSpriteRule(SpriteRuleUpdateDto spriteRuleUpdateDto)
        {
            await _commonAppService.UpdateSpriteRule(spriteRuleUpdateDto);
        }

        [HttpPost]
        [Route("UpdateWrapInfo")]
        public async Task UpdateWrapInfo(WrapInfoUpdateDto wrapInfoUpdateDto)
        {
            await _commonAppService.UpdateWrapInfo(wrapInfoUpdateDto);
        }
    }
}
