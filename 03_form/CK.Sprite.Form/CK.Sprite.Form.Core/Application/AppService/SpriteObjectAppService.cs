﻿using CK.Sprite.Framework;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    [Authorize]
    public class SpriteObjectAppService : AppService, ISpriteObjectAppService
    {
        private readonly SpriteObjectService _spriteObjectService;
        public CommonService _commonService => LazyGetRequiredService(ref commonService);
        private CommonService commonService;
        public SpriteObjectAppService(SpriteObjectService spriteObjectService)
        {
            _spriteObjectService = spriteObjectService;
        }

        public async Task AddSpriteObjectAsync(SpriteObjectCreateDto spriteObjectCreateDto)
        {
            await _spriteObjectService.AddSpriteObjectAsync(spriteObjectCreateDto);
        }

        public async Task UpdateSpriteObject(SpriteObjectUpdateDto spriteObjectUpdateDto)
        {
            await _spriteObjectService.UpdateSpriteObject(spriteObjectUpdateDto);
        }

        public async Task<List<SpriteObjectDto>> GetListSpriteObjectAsync(string applicationCode = "Default", int? keyType = null, string filter = default)
        {
            var spriteObjects = await _spriteObjectService.GetListSpriteObjectAsync(applicationCode, keyType, filter);
            return Mapper.Map<List<SpriteObjectDto>>(spriteObjects);
        }

        public async Task<SpriteObjectDto> GetSpriteObjectByIdAsync(Guid id)
        {
            var dbData = await _commonService.GetDataById<SpriteObject>("SpriteObjects", id);
            return Mapper.Map<SpriteObjectDto>(dbData);
        }

        #region ObjectProperty Operate

        public async Task AddObjectProperty(ObjectPropertyCreateDto objectPropertyCreateDto)
        {
            await _spriteObjectService.AddObjectProperty(objectPropertyCreateDto);
        }

        public async Task UpdateObjectProperty(ObjectPropertyUpdateDto objectPropertyUpdateDto)
        {
            await _spriteObjectService.UpdateObjectProperty(objectPropertyUpdateDto);
        }

        public async Task DeleteObjectProperty(Guid id)
        {
            await _spriteObjectService.DeleteObjectProperty(id);
        }

        public async Task<List<ObjectPropertyDto>> GetListObjectPropertyAsync(Guid objectId)
        {
            var objectPropertys = await _spriteObjectService.GetListObjectPropertyAsync(objectId);
            return Mapper.Map<List<ObjectPropertyDto>>(objectPropertys);
        }

        public async Task<ObjectPropertyDto> GetObjectPropertyByIdAsync(Guid id)
        {
            var dbData = await _commonService.GetDataById<ObjectProperty>("ObjectPropertys", id);
            return Mapper.Map<ObjectPropertyDto>(dbData);
        }

        #endregion

        #region ObjectMethod Operate

        public async Task AddObjectMethod(ObjectMethodCreateDto objectMethodCreateDto)
        {
            await _spriteObjectService.AddObjectMethodAsync(objectMethodCreateDto);
        }

        public async Task UpdateObjectMethod(ObjectMethodUpdateDto objectMethodUpdateDto)
        {
            await _spriteObjectService.UpdateObjectMethodAsync(objectMethodUpdateDto);
        }

        public async Task DeleteObjectMethod(Guid id)
        {
            await _spriteObjectService.DeleteObjectMethod(id);
        }

        public async Task<List<ObjectMethodDto>> GetListObjectMethodAsync(Guid objectId)
        {
            var results = await _spriteObjectService.GetListObjectMethodAsync(objectId);
            return Mapper.Map<List<ObjectMethodDto>>(results);
        }

        public async Task<ObjectMethodDto> GetObjectMethodByIdAsync(Guid id)
        {
            var dbData = await _commonService.GetDataById<ObjectMethod>("ObjectMethods", id);
            return Mapper.Map<ObjectMethodDto>(dbData);
        }

        #endregion
    }
}
