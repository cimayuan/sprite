﻿using AutoMapper;
using CK.Sprite.Framework;
using JetBrains.Annotations;
using Microsoft.Extensions.Options;
using MySqlX.XDevAPI.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    /// <summary>
    /// 客户端缓存基础数据，如果版本未更新，客户端直接读取缓存数据处理业务
    /// </summary>
    /// <typeparam name="T">缓存实体</typeparam>
    public interface IFrameworkCache<T> : ISingletonDependency
    {
        /// <summary>
        /// 最新版本号
        /// </summary>
        /// <returns></returns>
        string GetCurrentCacheVersion();

        /// <summary>
        /// 先判断最新版本号与客户端版本号是否一致，不一致返回缓存集合，否者不返回内容
        /// </summary>
        /// <returns></returns>
        List<T> GetBusinessCacheDtos();
    }
}
