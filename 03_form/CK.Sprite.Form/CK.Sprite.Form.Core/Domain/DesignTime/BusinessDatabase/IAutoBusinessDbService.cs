﻿using JetBrains.Annotations;
using System;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public interface IAutoBusinessDbService
    {
        /// <summary>
        /// 新建业务表单
        /// </summary>
        /// <param name="spriteObjectDto">Object对象</param>
        /// <returns></returns>
        Task<bool> CreateTable(SpriteObjectDto spriteObjectDto);

        Task<bool> AddObjectProperty(ObjectProperty objectProperty, string tableName);

        Task<bool> ModifyObjectProperty(ObjectProperty objectProperty, string tableName);

        Task<bool> DeleteObjectProperty(string propertyName, string tableName);
    }
}
