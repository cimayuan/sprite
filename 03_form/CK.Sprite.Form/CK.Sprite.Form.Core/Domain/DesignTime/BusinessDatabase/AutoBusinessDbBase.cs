﻿using CK.Sprite.Framework;
using JetBrains.Annotations;
using System;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public abstract class AutoBusinessDbServiceBase : IAutoBusinessDbService
    {
        protected IUnitOfWork _unitOfWork;

        public AutoBusinessDbServiceBase(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> CreateTable(SpriteObjectDto spriteObjectDto)
        {
            if (CheckTableExists(spriteObjectDto.Name))
            {
                throw new SpriteException("数据库表已经存在，请联系管理员！");
            }

            await DoCreateTable(spriteObjectDto);

            return await Task.FromResult(true);
        }

        /// <summary>
        /// 判断数据库表是否存在
        /// </summary>
        protected abstract bool CheckTableExists(string tableName);

        /// <summary>
        /// 执行创建表过程
        /// </summary>
        /// <param name="spriteObjectDto"></param>
        /// <returns></returns>
        protected abstract Task<bool> DoCreateTable(SpriteObjectDto spriteObjectDto);

        public abstract Task<bool> AddObjectProperty(ObjectProperty objectProperty, string tableName);

        public abstract Task<bool> ModifyObjectProperty(ObjectProperty objectProperty, string tableName);

        public abstract Task<bool> DeleteObjectProperty(string propertyName, string tableName);
    }
}
