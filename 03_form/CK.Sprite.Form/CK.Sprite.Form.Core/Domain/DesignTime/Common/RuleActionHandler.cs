﻿using CK.Sprite.Framework;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class RuleActionHandler : AbstractSpriteViewHandler, ITransientDependency
    {
        public async Task<long> AddRuleAction(RuleAction ruleAction, GuidRepositoryBase<RuleAction> ruleActionRepositoryParam = null)
        {
            var ruleActionRepository = ruleActionRepositoryParam == null ? new GuidRepositoryBase<RuleAction>(DesignUnitOfWork) : ruleActionRepositoryParam;
            return await ruleActionRepository.InsertAsync(ruleAction);
        }

        public async Task<bool> UpdateRuleAction(RuleAction ruleAction, GuidRepositoryBase<RuleAction> ruleActionRepositoryParam = null)
        {
            var ruleActionRepository = ruleActionRepositoryParam == null ? new GuidRepositoryBase<RuleAction>(DesignUnitOfWork) : ruleActionRepositoryParam;
            return await ruleActionRepository.UpdateAsync(ruleAction);
        }

        public async Task DeleteRuleAction(RuleAction ruleAction, GuidRepositoryBase<RuleAction> ruleActionRepositoryParam = null)
        {
            var ruleActionRepository = ruleActionRepositoryParam == null ? new GuidRepositoryBase<RuleAction>(DesignUnitOfWork) : ruleActionRepositoryParam;
            await ruleActionRepository.DeleteAsync(ruleAction);
        }
    }
}
