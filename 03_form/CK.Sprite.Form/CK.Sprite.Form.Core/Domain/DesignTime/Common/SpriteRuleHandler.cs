﻿using CK.Sprite.Framework;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class SpriteRuleHandler : AbstractSpriteViewHandler, ITransientDependency
    {
        public async Task<long> AddSpriteRule(SpriteRule spriteRule, GuidRepositoryBase<SpriteRule> spriteRuleRepositoryParam = null)
        {
            var spriteRuleRepository = spriteRuleRepositoryParam == null ? new GuidRepositoryBase<SpriteRule>(DesignUnitOfWork) : spriteRuleRepositoryParam;
            return await spriteRuleRepository.InsertAsync(spriteRule);
        }

        public async Task<bool> UpdateSpriteRule(SpriteRule spriteRule, GuidRepositoryBase<SpriteRule> spriteRuleRepositoryParam = null)
        {
            var spriteRuleRepository = spriteRuleRepositoryParam == null ? new GuidRepositoryBase<SpriteRule>(DesignUnitOfWork) : spriteRuleRepositoryParam;
            return await spriteRuleRepository.UpdateAsync(spriteRule);
        }

        public async Task DeleteSpriteRule(SpriteRule spriteRule, GuidRepositoryBase<SpriteRule> spriteRuleRepositoryParam = null)
        {
            var spriteRuleRepository = spriteRuleRepositoryParam == null ? new GuidRepositoryBase<SpriteRule>(DesignUnitOfWork) : spriteRuleRepositoryParam;
            await spriteRuleRepository.DeleteAsync(spriteRule);
        }
    }
}
