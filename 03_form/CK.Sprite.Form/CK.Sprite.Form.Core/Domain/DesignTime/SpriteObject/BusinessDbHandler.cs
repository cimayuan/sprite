﻿using CK.Sprite.Framework;
using CK.Sprite.ThirdContract;
using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class BusinessDbHandler : AbstractSpriteObjectHandler, ITransientDependency
    {
        private readonly ITenantConfigStore _tenantConfigStore;
        private readonly ICurrentTenant _currentTenant;
        public BusinessDbHandler(ITenantConfigStore tenantConfigStore,
            ICurrentTenant currentTenant)
        {
            _tenantConfigStore = tenantConfigStore;
            _currentTenant = currentTenant;
        }

        protected override async Task DoAddSpriteObjectAsync(SpriteObjectDto spriteObjectDto)
        {
            var businessConfig = await _tenantConfigStore.FindAsync(spriteObjectDto.ApplicationCode, _currentTenant.TenantCode);

            await _serviceProvider.DoDapperService(businessConfig, async (unitOfWork) =>
            {
                var autoBusinessDbService = (ConnectionFactory.GetConnectionProvider(businessConfig.ConnectionType) as IFormConnectionProvider).GetAutoBusinessDbService(unitOfWork);
                await autoBusinessDbService.CreateTable(spriteObjectDto);
            });
        }

        public async Task<bool> AddObjectProperty(ObjectProperty objectProperty, string tableName, string applicationCode)
        {
            var businessConfig = await _tenantConfigStore.FindAsync(applicationCode, _currentTenant.TenantCode);

            return await _serviceProvider.DoDapperService(businessConfig, async (unitOfWork) =>
            {
                var autoBusinessDbService = (ConnectionFactory.GetConnectionProvider(businessConfig.ConnectionType) as IFormConnectionProvider).GetAutoBusinessDbService(unitOfWork);
                return await autoBusinessDbService.AddObjectProperty(objectProperty, tableName);
            });
        }

        public async Task<bool> ModifyObjectProperty(ObjectProperty objectProperty, string tableName, string applicationCode)
        {
            var businessConfig = await _tenantConfigStore.FindAsync(applicationCode, _currentTenant.TenantCode);

            return await _serviceProvider.DoDapperService(businessConfig, async (unitOfWork) =>
            {
                var autoBusinessDbService = (ConnectionFactory.GetConnectionProvider(businessConfig.ConnectionType) as IFormConnectionProvider).GetAutoBusinessDbService(unitOfWork);
                return await autoBusinessDbService.ModifyObjectProperty(objectProperty, tableName);
            });
        }

        public async Task<bool> DeleteObjectProperty(string propertyName, string tableName, string applicationCode)
        {
            var businessConfig = await _tenantConfigStore.FindAsync(applicationCode, _currentTenant.TenantCode);

            return await _serviceProvider.DoDapperService(businessConfig, async (unitOfWork) =>
            {
                var autoBusinessDbService = (ConnectionFactory.GetConnectionProvider(businessConfig.ConnectionType) as IFormConnectionProvider).GetAutoBusinessDbService(unitOfWork);
                return await autoBusinessDbService.DeleteObjectProperty(propertyName, tableName);
            });
        }
    }
}
