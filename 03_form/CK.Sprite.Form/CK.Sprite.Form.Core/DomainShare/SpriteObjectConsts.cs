﻿using JetBrains.Annotations;
using System;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class SpriteObjectConsts
    {
        public const string DefaultCreateMethodName = "Create";
        public const string DefaultUpdateMethodName = "Update";
        public const string DefaultCreateOrUpdateMethodName = "CreateOrUpdate";
        public const string DefaultGetMethodName = "Get";
        public const string DefaultFact = "Fact";
        public const string DefaultDeleteMethodName = "Delete";
        public const string DefaultListMethodName = "List";
        public const string DefaultCreateRangeMethodName = "CreateRange";
        public const string DefaultUpdateWhereMethodName = "UpdateWhere";
        public const string DefaultGetWhereMethodName = "GetWhere";
        public const string DefaultDeleteWhereMethodName = "DeleteWhere";
        public const string DefaultListWhereMethodName = "ListWhere";
        public const string MultiListWhereMethodName = "MultiListWhere";
        public const string DefaultTreeListWhereMethodName = "TreeListWhere";
        public const string DefaultPageListMethodName = "PageList";
        public const string MultiPageListMethodName = "MultiPageList";
        public const string BatchCreateName = "BatchCreate";
    }
}
