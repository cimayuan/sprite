﻿using CK.Sprite.Framework;
using System;

namespace CK.Sprite.Form.Core
{
    [Dapper.Contrib.Extensions.Table("Controls")]
    public class Control : GuidEntityBase
    {
        /// <summary>
        /// 业务Id
        /// </summary>
        public Guid BusinessId { get; set; }

        /// <summary>
        /// 业务分类，Object,Form,View等
        /// </summary>
        public string BusinessCategory { get; set; }

        /// <summary>
        /// ListView视图方法执行=1,ListView查询按钮 = 2,ListView操作方法执行 = 3,ListView列表更多方法执行 = 4
        /// </summary>
        public EOperateType OperateType { get; set; }

        /// <summary>
        /// 控件名称
        /// </summary>
        public string ComponentName { get; set; }

        /// <summary>
        /// 控件属性
        /// </summary>
        public string ControlSettings { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public string Order { get; set; }
    }
}
