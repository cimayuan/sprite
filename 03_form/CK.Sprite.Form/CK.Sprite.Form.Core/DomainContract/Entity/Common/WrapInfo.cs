﻿using CK.Sprite.Framework;
using System;

namespace CK.Sprite.Form.Core
{
    [Dapper.Contrib.Extensions.Table("WrapInfos")]
    public class WrapInfo : GuidEntityBase
    {
        /// <summary>
        /// 业务Id
        /// </summary>
        public Guid BusinessId { get; set; }

        /// <summary>
        /// 业务分类，Object,Form,View等
        /// </summary>
        public string BusinessCategory { get; set; }

        /// <summary>
        /// 视图或者表单layout组件
        /// </summary>
        public string ContentComponent { get; set; }

        /// <summary>
        /// 视图或者表单Id
        /// </summary>
        public Guid? ObjId { get; set; }

        /// <summary>
        /// 存储List<ComponentName,WrapSetting>序列化后对象
        /// </summary>
        public string WrapConfigs { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
    }
}
