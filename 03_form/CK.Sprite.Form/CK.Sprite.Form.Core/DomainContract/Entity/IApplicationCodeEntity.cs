﻿using JetBrains.Annotations;
using System;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public interface IApplicationCodeEntity
    {
        /// <summary>
        /// 应用程序Code
        /// </summary>
        string ApplicationCode { get; set; }
    }
}
