﻿using CK.Sprite.Framework;
using System;

namespace CK.Sprite.Form.Core
{
    [Dapper.Contrib.Extensions.Table("FormCols")]
    public class FormCol : GuidEntityBase
    {
        /// <summary>
        /// 表单Id
        /// </summary>
        public Guid FormId { get; set; }

        /// <summary>
        /// 表单行Id
        /// </summary>
        public Guid FormRowId { get; set; }

        /// <summary>
        /// 列类型 Control=1,View=2,Form=3
        /// </summary>
        public EColType ColType { get; set; }

        /// <summary>
        /// 视图设置
        /// </summary>
        public string PropertySettings { get; set; }

        /// <summary>
        /// Label显示值
        /// </summary>
        public string LabelValue { get; set; }

        /// <summary>
        /// Label属性设置
        /// </summary>
        public string LabelSettings { get; set; }

        /// <summary>
        /// 控件名称
        /// </summary>
        public string ComponentName { get; set; }

        /// <summary>
        /// 控件属性
        /// </summary>
        public string ControlSettings { get; set; }

        /// <summary>
        /// 表达式树配置
        /// </summary>
        public string Express { get; set; }

        /// <summary>
        /// 视图或者表单Id
        /// </summary>
        public Guid? ObjId { get; set; }

        /// <summary>
        /// 存储List<ComponentName,WrapSetting>序列化后对象
        /// </summary>
        public string WrapConfigs { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public string Order { get; set; }
    }
}
