﻿using System;

namespace CK.Sprite.Form.Core
{
    [Dapper.Contrib.Extensions.Table("SpriteForms")]
    public class SpriteForm : DesignEntityBase
    {
        /// <summary>
        /// 表单英文名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 业务分类
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 版本
        /// </summary>
        public Guid Version { get; set; }

        /// <summary>
        /// NormalForm=1,TabForm=2,StepForm=3
        /// </summary>
        public EFormType FormType { get; set; }

        /// <summary>
        /// 视图设置
        /// </summary>
        public string PropertySettings { get; set; }

        /// <summary>
        /// 规则定义json存储
        /// </summary>
        public string Rules { get; set; }

        /// <summary>
        /// 弹出框等信息封装Json存储
        /// </summary>
        public string WrapInfos { get; set; }

        /// <summary>
        /// 视图控件设置
        /// </summary>
        public string Controls { get; set; }

        /// <summary>
        /// FormItems冗余数据
        /// </summary>
        public string FormItems { get; set; }

        /// <summary>
        /// 从菜单进入Wrap配置信息
        /// </summary>
        public string MenuWrapConfigs { get; set; }

        /// <summary>
        /// 是否为模板
        /// </summary>
        public bool IsTemplate { get; set; }
    }
}
