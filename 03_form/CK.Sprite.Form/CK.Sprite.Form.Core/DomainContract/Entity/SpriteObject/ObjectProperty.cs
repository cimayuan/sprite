﻿using CK.Sprite.Framework;
using System;

namespace CK.Sprite.Form.Core
{
    [Dapper.Contrib.Extensions.Table("ObjectPropertys")]
    public class ObjectProperty : GuidEntityBase
    {
        /// <summary>
        /// 字段名称(数据库字段名称，表唯一，新增后不能修改)
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// object外键
        /// </summary>
        public Guid ObjectId { get; set; }

        /// <summary>
        /// 字段类型
        /// </summary>
        public EFieldType FieldType { get; set; }

        /// <summary>
        /// 是否可空
        /// </summary>
        public bool IsNull { get; set; }

        /// <summary>
        /// 默认值
        /// </summary>
        public string DefaultValue { get; set; }

        /// <summary>
        /// 是否必填
        /// </summary>
        public bool IsRequired { get; set; }

        /// <summary>
        /// 是否唯一
        /// </summary>
        public bool IsUnique { get; set; }

        /// <summary>
        /// 字段长度
        /// </summary>
        public int Length { get; set; }
        
        /// <summary>
        /// 字段长度
        /// </summary>
        public int Length2 { get; set; }

        /// <summary>
        /// 排序（显示用）
        /// </summary>
        public string Order { get; set; }

        /// <summary>
        /// 默认表单自定义显示控件
        /// </summary>
        public string DefaultControl { get; set; }

        /// <summary>
        /// 默认表单自定义控件设置
        /// </summary>
        public string DefaultControlSettings { get; set; }

        /// <summary>
        /// 默认表单自定义验证
        /// </summary>
        public string DefaultValidate { get; set; }

        /// <summary>
        /// 字典（表单，列表，查询，高级查询自动生成时使用）
        /// </summary>
        public string Dict { get; set; }
    }
}
