﻿using CK.Sprite.Framework;
using Dapper.Contrib.Extensions;
using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public interface ISpriteViewRepository : IBaseSpriteRepository<SpriteView>
    {
        List<SpriteView> GetAllSpriteViewByApplicationCode(string applicationCode);

        /// <summary>
        /// 通用修改视图字段属性
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="viewId">视图Id</param>
        /// <param name="tableName">提取数据的表名</param>
        /// <param name="updateFieldName">需要修改的字段</param>
        /// <param name="viewIdField">提取表中视图Id字段名称</param>
        /// <param name="isOrder">是否排序</param>
        /// <returns></returns>
        Task CommonChangeChildDatas<T>(Guid viewId, string tableName, string updateFieldName, string viewIdField = "ViewId", bool isOrder = false);

        /// <summary>
        /// 视图行列修改时修改视图对应字段
        /// </summary>
        /// <param name="viewId">视图Id</param>
        /// <returns></returns>
        Task ChangeRowColChildDatas(Guid viewId);

        /// <summary>
        /// 规则修改时修改视图对应字段
        /// </summary>
        /// <param name="viewId">视图Id</param>
        /// <returns></returns>
        Task ChangeRuleActionChildDatas(Guid viewId);
    }
}
