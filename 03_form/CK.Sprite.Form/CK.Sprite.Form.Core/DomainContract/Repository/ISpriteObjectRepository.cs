﻿using CK.Sprite.Framework;
using Dapper.Contrib.Extensions;
using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public interface ISpriteObjectRepository : IBaseSpriteRepository<SpriteObject>
    {
        /// <summary>
        /// 根据applicationCode获取所有SpriteObject信息
        /// </summary>
        /// <param name="applicationCode"></param>
        /// <returns></returns>
        List<SpriteObject> GetAllSpriteObjectByApplicationCode(string applicationCode);

        /// <summary>
        /// 改变ObjectProperty调用
        /// </summary>
        /// <param name="objectId">object Id</param>
        /// <returns></returns>
        Task ChangeObjectProperty(Guid objectId);
    }
}
