﻿using CK.Sprite.Framework;
using Dapper.Contrib.Extensions;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public interface IRuntimeRepository : IRepository
    {
        /// <summary>
        /// 执行默认插入方法
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="paramValues">客户端传递的参数</param>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        /// <returns>插入主键{Result:result}</returns>
        Task<JObject> DoDefaultCreateMethodAsync(SpriteObjectDto spriteObjectDto, JObject paramValues, string sqlMethodContent = "");

        /// <summary>
        /// 执行默认修改方法
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="paramValues">客户端传递的参数</param>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        /// <returns>修改结果受影响行数，{Result:result}</returns>
        Task<JObject> DoDefaultUpdateMethodAsync(SpriteObjectDto spriteObjectDto, JObject paramValues, string sqlMethodContent = "");

        /// <summary>
        /// 执行默认删除方法
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="paramValues">客户端传递的参数(只有Id)</param>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        /// <returns>删除受影响行数，{Result:result}</returns>
        Task<JObject> DoDefaultDeleteMethodAsync(SpriteObjectDto spriteObjectDto, JObject paramValues, string sqlMethodContent = "");

        /// <summary>
        /// 执行默认Get方法
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="paramValues">客户端传递的参数(只有Id)</param>
        /// <param name="fields">字段集合</param>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        /// <returns>Get结果对象，{Result:result}</returns>
        Task<JObject> DoDefaultGetMethodAsync(SpriteObjectDto spriteObjectDto, JObject paramValues, JArray fields, string sqlMethodContent = "");

        /// <summary>
        /// 执行默认List方法
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="paramValues">方法参数</param>
        /// <param name="fields">字段集合</param>
        /// <param name="orderbys">排序</param>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        /// <returns>List结果对象，{Result:result}</returns>
        Task<JObject> DoDefaultListMethodAsync(SpriteObjectDto spriteObjectDto, JObject paramValues, JArray fields, JToken orderbys, string sqlMethodContent = "");

        /// <summary>
        /// 执行默认Update Where方法
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="paramValues">客户端传递的参数</param>
        /// <param name="sqlWheres">参数化Where条件</param>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        /// <returns></returns>
        Task<JObject> DoDefaultUpdateWhereMethodAsync(SpriteObjectDto spriteObjectDto, JObject paramValues, JToken sqlWheres, string sqlMethodContent = "");

        /// <summary>
        /// 执行默认Delete Where方法
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="paramValues">方法参数</param>
        /// <param name="sqlWheres">参数化Where条件</param>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        Task<JObject> DoDefaultDeleteWhereMethodAsync(SpriteObjectDto spriteObjectDto, JObject paramValues, JToken sqlWheres, string sqlMethodContent = "");

        /// <summary>
        /// 执行默认Get Where方法
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="paramValues">方法参数</param>
        /// <param name="sqlWheres">参数化Where条件</param>
        /// <param name="fields">字段集合</param>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        Task<JObject> DoDefaultGetWhereMethodAsync(SpriteObjectDto spriteObjectDto, JObject paramValues, JToken sqlWheres, JArray fields, string sqlMethodContent = "");

        /// <summary>
        /// 执行默认List Where方法
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="paramValues">方法参数</param>
        /// <param name="sqlWheres">参数化Where条件</param>
        /// <param name="fields">字段集合</param>
        /// <param name="orderbys">排序参数</param>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        /// <returns>List Where结果对象，{Result:result}</returns>
        Task<JObject> DoDefaultListWhereMethodAsync(SpriteObjectDto spriteObjectDto, JObject paramValues, JToken sqlWheres, JArray fields, JToken orderbys, string sqlMethodContent = "");

        /// <summary>
        /// 多表联合查询List获取
        /// </summary>
        /// <param name="dictAliasInfos">aliasInfos格式为：table1:a;table2:b，且与joinInfos需要完全对应</param>
        /// <param name="joinInfos">连接条件，如：table1 a JOIN table2 b ON a.Id=b.Id</param>
        /// <param name="paramValues">方法参数</param>
        /// <param name="sqlWheres">参数化Where条件</param>
        /// <param name="fields">字段集合</param>
        /// <param name="orderbys">排序参数</param>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        /// <returns>List Where结果对象，{Result:result}</returns>
        Task<JObject> DoMultiListWhereMethodAsync(Dictionary<string, SpriteObjectDto> dictAliasInfos, JToken joinInfos, JObject paramValues, JToken sqlWheres, JArray fields, JToken orderbys, string sqlMethodContent = "");

        /// <summary>
        /// 执行默认Page List方法
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="paramValues">方法参数</param>
        /// <param name="sqlWheres">参数化Where条件</param>
        /// <param name="fields">字段集合</param>
        /// <param name="orderbys">排序参数</param>
        /// <param name="maxResultCount">最大行数</param>
        /// <param name="skipCount">跳过行数</param>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        /// <returns></returns>
        Task<JObject> DoDefaultPageListMethodAsync(SpriteObjectDto spriteObjectDto, JObject paramValues, JToken sqlWheres, JArray fields, JToken orderbys, JToken maxResultCount, JToken skipCount, string sqlMethodContent = "");

        /// <summary>
        /// 多表联合分页Page List方法
        /// </summary>
        /// <param name="dictAliasInfos">aliasInfos格式为：table1:a;table2:b，且与joinInfos需要完全对应</param>
        /// <param name="joinInfos">连接条件，如：table1 a JOIN table2 b ON a.Id=b.Id</param>
        /// <param name="paramValues">方法参数</param>
        /// <param name="sqlWheres">参数化Where条件</param>
        /// <param name="fields">字段集合</param>
        /// <param name="orderbys">排序参数</param>
        /// <param name="maxResultCount">最大行数</param>
        /// <param name="skipCount">跳过行数</param>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        /// <returns></returns>
        Task<JObject> DoMultiPageListMethodAsync(Dictionary<string, SpriteObjectDto> dictAliasInfos, JToken joinInfos, JObject paramValues, JToken sqlWheres, JArray fields, JToken orderbys, JToken maxResultCount, JToken skipCount, string sqlMethodContent = "");

        /// <summary>
        /// 通用方法执行
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="paramValues">方法参数</param>
        /// <param name="sqlWheres">参数化Where条件</param>
        /// <param name="fields">字段集合</param>
        /// <param name="orderbys">排序参数</param>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        /// <returns></returns>
        Task<JObject> DoSqlMethodAsync(SpriteObjectDto spriteObjectDto, JObject paramValues, JToken sqlWheres, JArray fields, JToken orderbys, string sqlMethodContent);

        /// <summary>
        /// 通用批量新增方法
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="arrayParamValues">方法参数</param>
        /// <returns></returns>
        Task<JObject> DoBatchCreateMethodAsync(SpriteObjectDto spriteObjectDto, JArray arrayParamValues);

        /// <summary>
        /// 批量更新方法
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="arrayParamValues">方法参数</param>
        /// <returns></returns>
        Task<JObject> DoBatchUpdateMethodAsync(SpriteObjectDto spriteObjectDto, JArray arrayParamValues);

        /// <summary>
        /// 获取数据库唯一字段信息（导入判断重复用）
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="uniqFieldInfo">唯一字段信息，联合字段为Concat函数拼接值，否则为具体字段</param>
        /// <param name="uniqValues">Excel表中数据</param>
        /// <returns></returns>
        Task<JObject> DoGetUniqInfos(SpriteObjectDto spriteObjectDto, string uniqFieldInfo, List<string> uniqValues);

        /// <summary>
        /// 获取对象默认远程调用数据
        /// </summary>
        /// <param name="spriteObjectDto">Object对象信息</param>
        /// <param name="filter">查询过滤条件</param>
        /// <returns></returns>
        Task<JArray> DoGetRemoteSelectCall(SpriteObjectDto spriteObjectDto, string filter);
    }
}
