﻿using CK.Sprite.Framework;
using Dapper.Contrib.Extensions;
using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public interface ISpriteCommonRepository : IBaseSpriteRepository<SpriteView>
    {
        Task DeleteSpriteFormOrView(List<Guid> ids);

        /// <summary>
        /// 通用List查询
        /// </summary>
        /// <typeparam name="T">返回实体</typeparam>
        /// <param name="tableName">表名称</param>
        /// <param name="queryWhereModels">查询条件</param>
        /// <param name="fields">指定查询字段</param>
        /// <returns></returns>
        Task<List<T>> GetCommonList<T>(string tableName, List<QueryWhereModel> queryWhereModels, string fields = default);

        Task<List<T>> GetCommonList2<T>(string tableName, ExpressSqlModel expressSqlModel, string fields = default);

        /// <summary>
        /// 获取单个实体信息
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="tableName">表名称</param>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task<T> GetDataById<T>(string tableName, Guid id);

        Task<List<T>> GetDatasById<T>(string tableName, Guid id, string fieldName = "");

        Task DeleteById(string tableName, Guid id, string fieldName = "");

        Task DeleteByIds(string tableName, List<Guid> ids, string fieldName = "");
    }
}
