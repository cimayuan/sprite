﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    [Serializable]
    public class FormViewVueInfos
    {
        public FormViewVueInfos()
        {
            FormDatas = new List<SpriteFormVueDto>();
            ViewDatas = new List<SpriteViewVueDto>();
            Dicts = new List<DictVueDto>();
        }

        /// <summary>
        /// 最新表单信息
        /// </summary>
        public List<SpriteFormVueDto> FormDatas { get; set; }

        /// <summary>
        /// 最新视图信息
        /// </summary>
        public List<SpriteViewVueDto> ViewDatas { get; set; }

        /// <summary>
        /// 最新字典信息
        /// </summary>
        public List<DictVueDto> Dicts { get; set; }
        
        /// <summary>
        /// 最新字典版本
        /// </summary>
        public string DictVersion { get; set; }
    }

    public class RelationInfoInputs
    {
        public string ApplicationCode { get; set; }
        public Guid FormId { get; set; }
        public List<RelationInfoInput> RelationInfos { get; set; }
        public string DictVersion { get; set; }
    }

    public class RelationInfoInput
    {
        /// <summary>
        /// 1=表单，2=视图
        /// </summary>
        public int RelationType { get; set; }

        /// <summary>
        /// 主键
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 版本号
        /// </summary>
        public Guid Version { get; set; }
    }
}
