﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    [Serializable]
    public class ListViewCustomerColumnDto : ListViewCustomerColumnUpdateDto
    {
        /// <summary>
        /// 视图Id
        /// </summary>
        public Guid ViewId { get; set; }
    }

    [Serializable]
    public class ListViewCustomerColumnCreateDto : ListViewCustomerColumnUpdateDto
    {
        /// <summary>
        /// 视图Id
        /// </summary>
        public Guid ViewId { get; set; }
    }

    [Serializable]
    public class ListViewCustomerColumnUpdateDto
    {
        /// <summary>
        /// 实体主键，新增时不需要赋值
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 编辑状态列组件
        /// </summary>
        public string EditComponentName { get; set; }

        /// <summary>
        /// 编辑状态组件设置
        /// </summary>
        public string EditSettings { get; set; }

        /// <summary>
        /// 控件名称
        /// </summary>
        public string ComponentName { get; set; }

        /// <summary>
        /// 控件属性
        /// </summary>
        public string ControlSettings { get; set; }

        /// <summary>
        /// 通常为字段名称
        /// </summary>
        public string DataIndex { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

    }
}
