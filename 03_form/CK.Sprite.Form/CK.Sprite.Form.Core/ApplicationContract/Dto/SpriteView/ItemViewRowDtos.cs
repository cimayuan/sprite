﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    [Serializable]
    public class ItemViewRowDto : ItemViewRowUpdateDto
    {
        /// <summary>
        /// 视图Id
        /// </summary>
        public Guid ViewId { get; set; }
    }

    [Serializable]
    public class ItemViewRowCreateDto : ItemViewRowUpdateDto
    {
        /// <summary>
        /// 视图Id
        /// </summary>
        public Guid ViewId { get; set; }
    }

    [Serializable]
    public class ItemViewRowUpdateDto
    {
        /// <summary>
        /// 实体主键，新增时不需要赋值
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 视图设置
        /// </summary>
        public string PropertySettings { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public string Order { get; set; }

    }
}
