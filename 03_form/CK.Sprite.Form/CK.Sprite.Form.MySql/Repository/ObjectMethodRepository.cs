﻿using CK.Sprite.Form.Core;
using CK.Sprite.Framework;
using Dapper;
using Dapper.Contrib.Extensions;
using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.MySql
{
    public class ObjectMethodRepository : BaseSpriteRepository<ObjectMethod>, IObjectMethodRepository
    {
        public ObjectMethodRepository(IUnitOfWork unitOfWork) : base(unitOfWork) { }

        #region Sql

        private const string SqlGetAllObjectMethodByApplicationCode = @"SELECT * FROM ObjectMethods WHERE ApplicationCode=@ApplicationCode;";

        #endregion

        public List<ObjectMethod> GetAllObjectMethodByApplicationCode(string applicationCode)
        {
            return _unitOfWork.Connection.Query<ObjectMethod>(SqlGetAllObjectMethodByApplicationCode, new { ApplicationCode = applicationCode }, _unitOfWork.Transaction).ToList();
        }
    }
}
