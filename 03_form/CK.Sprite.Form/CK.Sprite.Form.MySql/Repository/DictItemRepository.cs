﻿using CK.Sprite.Form.Core;
using CK.Sprite.Framework;
using Dapper;
using Dapper.Contrib.Extensions;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.MySql
{
    public class DictItemRepository : BaseSpriteRepository<DictItem>, IDictItemRepository
    {
        public DictItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork) { }

        public async Task<List<DictItem>> GetDictItemsByDictCode(string dictCode)
        {
            var strSql = "SELECT * FROM DictItems WHERE DictCode=@DictCode;";
            var results = await _unitOfWork.Connection.QueryAsync<DictItem>(strSql, new { DictCode = dictCode }, _unitOfWork.Transaction);
            return results.ToList();
        }

        public async Task DeleteDictItemsByDictCode(string dictCode)
        {
            var strSql = "DELETE FROM DictItems WHERE DictCode=@DictCode;";
            await _unitOfWork.Connection.ExecuteAsync(strSql, new { DictCode = dictCode }, _unitOfWork.Transaction);
        }
    }
}
