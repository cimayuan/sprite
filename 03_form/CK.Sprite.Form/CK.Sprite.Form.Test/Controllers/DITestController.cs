﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CK.Sprite.Form.Test.Models;

namespace CK.Sprite.Form.Test.Controllers
{
    public class DITestController : Controller
    {
        private readonly ILogger<DITestController> _logger;
        private readonly IDITest _dITest;

        public DITestController(ILogger<DITestController> logger, IDITest dITest)
        {
            _logger = logger;
            _dITest = dITest;
        }

        public string Index()
        {
            var testDi = ServiceLocator.ServiceProvider.GetService<IDITest>("CK.Sprite.Form.Test.IDITest,CK.Sprite.Form.Test");
            return testDi.GetString();
            //return _dITest.GetString();
        }
    }
}
