﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace CK.Sprite.Framework
{
    /// <summary>
    ///  远程控件绑定下拉值
    /// </summary>
    public class ValueText
    {
        public string text { get; set; }
        public string value { get; set; }
    }

    public class RemoteSelectInfo
    {
        public string fields { get; set; }
        public string whereFields { get; set; }
        public string orderInfo { get; set; }
        public int resultCount { get; set; }
        public string script { get; set; }
    }
}
