﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CK.Sprite.Framework
{
    public class PagedDto
    {
        /// <summary>
        /// 跳过多少行
        /// </summary>
        public int SkipCount { get; set; }
        /// <summary>
        /// 每页最大行数
        /// </summary>
        public int MaxResultCount { get; set; }
    }

    public class PagedAndSortDto : PagedDto
    {
        /// <summary>
        /// 排序
        /// </summary>
        public string Sorting { get; set; }
    }
}
